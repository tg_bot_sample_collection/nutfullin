#!venv/bin/python
import asyncio
import logging
import aiogram.utils.markdown as fmt
from aiogram.utils.exceptions import BotBlocked
from aiogram import Bot, Dispatcher, executor, types
from os import getenv
from sys import exit
from aiogram import types
import requests
from bs4 import BeautifulSoup
import requests
import lxml
from aiogram.dispatcher.filters import Text
import aiohttp

url = "https://google-translate1.p.rapidapi.com/language/translate/v2"

payload = "q=Hello%2C%20world!&target=ru&source=en"
headers = {
    "content-type": "application/x-www-form-urlencoded",
    "Accept-Encoding": "application/gzip",
    "X-RapidAPI-Key": "87d1c7d3c8msh19c44bf852ae085p1b206cjsnd7e1b00c8218",
    "X-RapidAPI-Host": "google-translate1.p.rapidapi.com"
}
#
# response = requests.request("POST", url, data=payload, headers=headers)
#
# print(response.text)


bot_token = getenv("BOT_TOKEN")
if not bot_token:
    exit("Error: no token provided")

bot = Bot(token=bot_token, parse_mode=types.ParseMode.HTML)

# Диспетчер для бота
dp = Dispatcher(bot)
# Включаем логирование, чтобы не пропустить важные сообщения
logging.basicConfig(level=logging.INFO)


async def get_lesson_title(lesson_number):
    link = 'https://pydocs.ru/parsing-na-python/'
    html = await get_html(link)
    data = get_data(html, lesson_number)
    return data


async def get_html(link):
    async with aiohttp.ClientSession() as session:
        async with session.get(link) as response:
            html = await response.text()
            return html


def get_data(html, lesson_number):
    if lesson_number == 1:
        return get_first_lesson_title(html)
    if lesson_number == 2:
        return get_second_lesson_title(html)


def get_first_lesson_title(html):
    soup = BeautifulSoup(html, 'lxml')
    return soup.find(
        'div',
        id="tc-page-wrap"
    ).find(
        'header'
    ).find(
        'div'
    ).find(
        'div'
    ).find(
        'div'
    ).find(
        'div'
    ).find(
        'nav',
        id='topbar-nav'
    ).find(
        'div'
    ).find(
        'ul',
        id='topbar-menu'
    ).find(
        'li',
        id='menu-item-19'
    ).find(
        'a'
    ).find(
        'span'
    ).text


def get_second_lesson_title(html):
    soup = BeautifulSoup(html, 'lxml')
    return soup.find(
        'div',
        id="tc-page-wrap"
    ).find(
        'header'
    ).find(
        'div'
    ).find(
        'div'
    ).find(
        'div'
    ).find(
        'div'
    ).find(
        'nav',
        id='topbar-nav'
    ).find(
        'div'
    ).find(
        'ul',
        id='topbar-menu'
    ).find(
        'li',
        id='menu-item-19'
    ).find(
        'a'
    ).find(
        'span'
    ).text


# Хэндлер на команду /test1
@dp.message_handler(commands="test1")
async def cmd_test1(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup()
    button_1 = types.KeyboardButton(text="Урок 1")
    keyboard.add(button_1)
    button_2 = "Урок 2"
    keyboard.add(button_2)
    # button_1.text = await get_lesson_title(1)
    # button_2.text = await get_lesson_title(2)
    await message.answer("Выберете название какого урока необходимо получить", reply_markup=keyboard)


@dp.message_handler(Text(equals="Урок 1"))
async def lesson_one(message: types.Message):
    await message.reply(await get_lesson_title(1))


@dp.message_handler(Text(equals="Урок 2"))
async def lesson_one(message: types.Message):
    await message.reply(await get_lesson_title(2))


if __name__ == "__main__":
    # Запуск бота
    executor.start_polling(dp, skip_updates=True)
